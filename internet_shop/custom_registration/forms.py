from django import forms
from django.contrib.auth.models import User

from registration_email.forms import EmailRegistrationForm, generate_username


class CustomEmailRegistrationForm(EmailRegistrationForm):

    your_name = None

    first_name = forms.CharField()
    last_name = forms.CharField()

    def save(self, commit=True):
        user = User()
        user.username = generate_username(self.cleaned_data['email'])
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        user.email = self.cleaned_data['email']
        user.set_password(self.cleaned_data['password1'])
        if commit:
            user.save()
        return user
