"""internet_shop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings

from registration.backends.simple.views import RegistrationView
from custom_registration.forms import CustomEmailRegistrationForm

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('core.urls')),

    url(
        r'^accounts/register/$',
        RegistrationView.as_view(
            template_name='registration/registration_form.html',
            form_class=CustomEmailRegistrationForm,
            get_success_url=getattr(
                settings, 'REGISTRATION_EMAIL_REGISTER_SUCCESS_URL',
                lambda user: '/'),
        ),
        name='registration_register',
    ),

    url(r'^accounts/', include('registration_email.backends.default.urls')),
    url(r'^accounts/', include('registration.urls')),
]
