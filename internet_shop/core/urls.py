from django.conf.urls import url, include

from views import index, get_catalog, get_tags, add_to_cart, remove_from_cart, cart, get_cart, make_order

urlpatterns = [
    url(r'^$', index, name='index'),

    url(r'^get_catalog/(?P<category_id>[0-9]+)/$', get_catalog, name='get_catalog'),
    url(r'^get_catalog/$', get_catalog, name='get_catalog'),

    url(r'^get_tags/(?P<category_id>[0-9]+)/$', get_tags, name='get_tags'),
    url(r'^get_tags/$', get_tags, name='get_tags'),

    url(r'^add_to_cart/(?P<product_id>[0-9]+)/$', add_to_cart, name='add_to_cart'),
    url(r'^remove_from_cart/(?P<product_id>[0-9]+)/$', remove_from_cart, name='remove_from_cart'),

    url(r'^cart/$', cart, name='cart'),
    url(r'^get_cart/$', get_cart, name='get_cart'),

    url(r'^make_order/$', make_order, name='make_order'),
]