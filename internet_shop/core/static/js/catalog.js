var
    catalog = $("#catalog"),
    tags = $("#tags"),
    // Выбранная категория
    current_catalog_item = undefined,
    // Текущая страница каталога
    current_page = 1,
    // Отмеченные теги
    toggle_tags = [];

var
    cart_count = $("#cart-count"),
    cart_summary = $("#cart-summary");

// Добавление в корзину
$(".btn-cart").live('click', function(event){
    $.ajax({
        url: "/add_to_cart/" + event.target.id + "/",
        success: function(result) {
            $(cart_count).html(result.cart_count)
            $(cart_summary).html(result.cart_summary)
        }
    });
});

// Обновление каталога
function update_catalog(category_id, page_number, filter_tags) {
     var
        request_url = "/get_catalog/";
     if (category_id)
        request_url = "/get_catalog/" + category_id + "/";
     var
        parameters = []
     if (page_number)
        parameters.push("page=" + page_number);
     if ((filter_tags) && (filter_tags.length > 0))
        parameters.push("tags=" + filter_tags.join(','));
     if (parameters)
        request_url += "?" + parameters.join('&');
     $.ajax({
        url: request_url,
        success: function(result) {
            $(catalog).html(result)
        }
     });
};

// Обновление тегов по выбранной категории
function update_tags(category_id) {
    var
        request_url = "/get_tags/";
    if (category_id)
        request_url = "/get_tags/" + category_id + "/";
    $.ajax({
        url: request_url,
        success: function(result) {
            $(tags).html(result)
        }
    });
};

function update(category_id) {
    update_catalog(category_id)
    update_tags(category_id)
}

function clear_catalog_state() {
    current_catalog_item = undefined
    toggle_tags = []
    current_page = 1
};

$(document).ready( function() {
    clear_catalog_state()
    update()
});

// Переход на новую категорию
$(".catalog-item").click( function(event) {
    clear_catalog_state()
    current_catalog_item = $(this).attr('id')
    update(current_catalog_item)
    return false
});

// Фильтрация по тегам
$(".toggle-tag").live('click', function(event){
    current_page = 1
    toggle_id = event.target.id
    if ($(event.target).hasClass('active'))
        toggle_tags.push(toggle_id)
    else {
        var index = toggle_tags.indexOf(toggle_id);
        if (index > -1) {
            toggle_tags.splice(index, 1);
        }
    }
    update_catalog(current_catalog_item, current_page, toggle_tags)
});

// Навигация по элементам каталога внутри выбранной категории
function go_to_page(page_number) {
    current_page = page_number
    update_catalog(current_catalog_item, page_number, toggle_tags)
};

$(".btn-catalog-prev").live('click', function(event){
    go_to_page(event.target.id)
});

$(".btn-catalog-next").live('click', function(event){
    go_to_page(event.target.id)
});