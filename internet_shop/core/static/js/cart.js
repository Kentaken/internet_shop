var
    cart = $('#cart'),
    // Количество элементов в корзине
    cart_count = $("#cart-count"),
    // Стоимость всех товаров  в корзине
    cart_summary = $("#cart-summary");

// Обновление корзины
function update_cart() {
     $.ajax({
        url: "/get_cart/",
        success: function(result) {
            $(cart).html(result)
        }
     });
};

// Удаление позиции в корзине
$(".btn-cart-remove-item").live('click', function(event){
    $.ajax({
        url: "/remove_from_cart/" + event.target.id + "/",
        success: function(result) {
            $(cart_count).html(result.cart_count)
            $(cart_summary).html(result.cart_summary)
            update_cart()
        }
    });
});

$(document).ready( function() {
    update_cart()
});