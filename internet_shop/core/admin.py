# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.models import User
from django_mptt_admin.admin import DjangoMpttAdmin
from django.http import HttpResponse
import unicodecsv as csv

from models import Category, Product, Tag, Order
from cart.models import Cart, Item


class CategoryAdmin(DjangoMpttAdmin):
    tree_auto_open = 0
    list_display = ('name',)
    ordering = ('name',)

    def has_change_permission(self, request, obj=None):
        return request.user.is_superuser


class CartAdmin(admin.ModelAdmin):
    list_display = ('id', 'creation_date', 'checked_out',)
    list_filter = ('checked_out',)


class ItemAdmin(admin.ModelAdmin):
    list_display = ('id', 'cart', 'quantity', 'unit_price', 'object_id',)


def export_task(modeladmin, request, queryset):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename=orders.csv'

    w = csv.writer(response, encoding='cp1251')
    w.writerow([u'Дата заказа', u'Имя', u'Фамилия', u'Статус заказа'])
    for obj in queryset:
        user = User.objects.get(id=obj.user.id)
        w.writerow((
            obj.datetime,
            user.first_name,
            user.last_name,
            obj.status,
        ))
        w.writerow((u'Заказ:',))
        cart_items = Item.objects.filter(cart_id=obj.cart.id)
        summary = 0
        for item in cart_items:
            product = item.get_product()
            w.writerow([
                product.name,
                item.quantity,
                item.quantity * item.unit_price,
            ])
            summary += item.quantity * item.unit_price
        w.writerow((u'Всего:', summary,))
        w.writerow(('',))
    return response
export_task.short_description = u'Экспорт в csv'


class OrderAdmin(admin.ModelAdmin):
    list_display = ['datetime', 'id', 'status']
    ordering = ['datetime']
    actions = [export_task]

admin.site.register(Cart, CartAdmin)
admin.site.register(Item, ItemAdmin)

admin.site.register(Category, CategoryAdmin)
admin.site.register(Product)
admin.site.register(Tag)
admin.site.register(Order, OrderAdmin)
