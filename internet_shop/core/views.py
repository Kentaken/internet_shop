# -*- coding: utf-8 -*-

from django.shortcuts import render, render_to_response, redirect
from django.http import JsonResponse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from cart.cart import Cart

from models import Category, Product, Tag, Order
import settings_local


def index(request):
    """
    Главная страница
    """
    return render_to_response(
        "index.html",
        {
            'nodes': Category.objects.all(),
            'cart': Cart(request),
            'user': request.user
        },
    )


def get_tags(request, category_id=None):
    """
    Возвращает теги продуктов в выбранной категории
    """
    if category_id is not None:
        try:
            current_category = Category.objects.get(id=category_id)
        except Category.DoesNotExist:
            current_category = None
        categories = current_category.get_descendants(include_self=True).values_list('id', flat=True)
    else:
        categories = Category.objects.all().values_list('id', flat=True)

    tags = Tag.objects.filter(product__category_id__in=categories).distinct()

    return render_to_response(
        "tags.html",
        {
            'tags': tags
        },
    )


def get_catalog(request, category_id=None):
    """
    Возвращает продукты в выбранной категории
    """
    current_category = None
    if category_id is not None:
        try:
            current_category = Category.objects.get(id=category_id)
        except Category.DoesNotExist:
            current_category = None
        categories = current_category.get_descendants(include_self=True).values_list('id', flat=True)
    else:
        categories = Category.objects.all().values_list('id', flat=True)

    products = Product.objects.filter(category_id__in=categories)
    tags = request.GET.get('tags')
    if tags is not None:
        tags = tags.split(',')
        products = products.filter(tags__in=tags)

    paginator = Paginator(products, settings_local.CATALOG_ITEMS_PER_PAGE)

    page = request.GET.get('page')
    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    return render_to_response(
        "catalog.html",
        {
            'nodes': Category.objects.all(),
            'current_category': current_category,
            'products': products,
            'cart': Cart(request),
            'user': request.user
        },
    )


def add_to_cart(request, product_id):
    """
    Добавление продукта в корзину
    """
    product = Product.objects.get(id=product_id)
    cart = Cart(request)
    cart.add(product, product.price, 1)
    return JsonResponse({
        'cart_count': cart.count(),
        'cart_summary': cart.summary()
    })


def remove_from_cart(request, product_id):
    """
    Удаление продукта из корзины
    """
    product = Product.objects.get(id=product_id)
    cart = Cart(request)
    cart.remove(product)
    return JsonResponse({
        'cart_count': cart.count(),
        'cart_summary': cart.summary()
    })


def cart(request):
    """
    Страница корзины
    """
    return render_to_response(
        "cart.html",
        {
            'user': request.user,
            'cart': Cart(request)
        },
    )


def get_cart(request):
    """
    Возвращает элементы корзины
    """
    return render_to_response(
        "cart_items.html",
        {
            'cart': Cart(request)
        },
    )


def make_order(request):
    """
    Осуществление заказа
    """
    if request.user.is_authenticated():
        cart = Cart(request)
        order = Order()
        order.user = request.user
        order.cart = cart.cart
        order.save()
        cart.new(request)
        return render_to_response(
            "order_success.html",
            {
                'cart': cart,
            }
        )
    else:
        return redirect("/accounts/login")
