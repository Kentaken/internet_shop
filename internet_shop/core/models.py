# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from six import python_2_unicode_compatible
from datetime import datetime

from django.db import models
from django.contrib.auth.models import User
from mptt.models import MPTTModel, TreeForeignKey
from cart.models import Cart


@python_2_unicode_compatible
class Tag(models.Model):

    name = models.CharField(verbose_name=u'Тег', max_length=32)

    def __str__(self):
        return self.name or ''

    class Meta:
        verbose_name = u'Тег'
        verbose_name_plural = u'Теги'


@python_2_unicode_compatible
class Category(MPTTModel):

    name = models.CharField(max_length=64, verbose_name=u'Название')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')

    def __str__(self):
        return self.name or ''

    class Meta:
        verbose_name = u'Категория'
        verbose_name_plural = u'Категории'


@python_2_unicode_compatible
class Product(models.Model):

    name = models.CharField(max_length=255, verbose_name=u'Название')
    price = models.DecimalField(max_digits=18, decimal_places=2, verbose_name=u'Цена')
    tags = models.ManyToManyField(Tag, verbose_name=u'Теги')
    category = models.ForeignKey(Category, verbose_name=u'Категория')

    def __str__(self):
        return self.name or ''

    class Meta:
        verbose_name = u'Продукт'
        verbose_name_plural = u'Продукты'


@python_2_unicode_compatible
class Order(models.Model):

    STATUS_NEW = 'new'
    STATUS_DONE = 'done'

    STATUSES = (
        (STATUS_NEW, u'Новый'),
        (STATUS_DONE, u'Завершено'),
    )

    user = models.ForeignKey(User, verbose_name=u'Пользователь')
    cart = models.ForeignKey(Cart, verbose_name=u'Корзина')
    status = models.CharField(max_length=30, choices=STATUSES, default=STATUS_NEW, verbose_name=u'Статус')
    datetime = models.DateTimeField(default=datetime.now(), verbose_name=u'Дата создания')

    def __str__(self):
        return "{0} {1} {2}".format(str(self.id), self.user.first_name, self.user.last_name)

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

